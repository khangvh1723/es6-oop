export class FormDetailComponent {
    constructor() {
        this.btnAddPerson = document.getElementById("btnAddPerson");
        this.btnUpdate = document.getElementById("btnUpdate");
        this.detailsForm = document.getElementById("details__form");
    }
    renderStudentForm() {
        let contentHTML = `
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"
                ><i class="fa fa-money" aria-hidden="true"></i
              ></span>
            </div>
            <input
              type="number"
              name="math"
              id="math"
              class="form-control input-sm"
              placeholder="math"
            />
          </div>
          <span class="sp-thongbao" id="tbLuongCB"></span>
        </div>
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"
                ><i class="fa fa-clock-o" aria-hidden="true"></i
              ></span>
            </div>
            <input
              type="number"
              name="physics"
              id="physics"
              class="form-control input-sm"
              placeholder="Physics"
            />
          </div>
          <span class="sp-thongbao" id="tbGiolam"></span>
        </div>
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"
                ><i class="fa fa-clock-o" aria-hidden="true"></i
              ></span>
            </div>
            <input
              type="number"
              name="chemistry"
              id="chemistry"
              class="form-control input-sm"
              placeholder="Chemistry"
            />
          </div>
          <span class="sp-thongbao" id="tbGiolam"></span>
        </div>
        `;
        this.detailsForm.innerHTML = contentHTML;
        this.btnAddPerson.setAttribute( "onClick", "createStudent()" );
        this.btnUpdate.setAttribute( "onClick", "updateStudent()" );
    }
    renderEmployeeForm() {
        let contentHTML = `
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"
                ><i class="fa fa-money" aria-hidden="true"></i
              ></span>
            </div>
            <input
              type="number"
              name="workingDays"
              id="workingDays"
              class="form-control input-sm"
              placeholder="workingDays"
            />
          </div>
          <span class="sp-thongbao" id="tbLuongCB"></span>
        </div>
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"
                ><i class="fa fa-clock-o" aria-hidden="true"></i
              ></span>
            </div>
            <input
              type="number"
              name="wage"
              id="wage"
              class="form-control input-sm"
              placeholder="Wage"
            />
          </div>
          <span class="sp-thongbao" id="tbGiolam"></span>
        </div>
        `;
        this.detailsForm.innerHTML = contentHTML;
        this.btnAddPerson.setAttribute( "onClick", "createEmployee()" );
        this.btnUpdate.setAttribute( "onClick", "updateEmployee()" );
    }
    renderCustomerForm() {
      let contentHTML = `
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"
              ><i class="fa fa-users" aria-hidden="true"></i
            ></span>
          </div>
          <input
            type="text"
            name="company"
            id="company"
            class="form-control input-sm"
            placeholder="company"
          />
        </div>
        <span class="sp-thongbao" id="tbLuongCB"></span>
      </div>
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"
              ><i class="fa fa-money" aria-hidden="true"></i
            ></span>
          </div>
          <input
            type="number"
            name="invoice"
            id="invoice"
            class="form-control input-sm"
            placeholder="Invoice"
          />
        </div>
        <span class="sp-thongbao" id="tbGiolam"></span>
      </div>
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"
              ><i class="fa fa-comment" aria-hidden="true"></i
            ></span>
          </div>
          <input
            type="text"
            name="feedback"
            id="feedback"
            class="form-control input-sm"
            placeholder="Feedback"
          />
        </div>
        <span class="sp-thongbao" id="tbGiolam"></span>
      </div>
      `;
      this.detailsForm.innerHTML = contentHTML;
      this.btnAddPerson.setAttribute( "onClick", "createCustomer()" );
      this.btnUpdate.setAttribute( "onClick", "updateCustomer()" );
  }
}