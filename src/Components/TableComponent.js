export class TableComponent {
    renderStudentList = (StudentList) => {
        let tbodyContent = ``;
        StudentList.map(item=>{
            let content =  `
            <tr>
                <th>${item.id}</th>
                <td>${item.name}</td>
                <td>${item.email}</td>
                <td>${item.address}</td>
                <td>${item.math}</td>
                <td>${item.physics}</td>
                <td>${item.chemistry}</td>
                <td>${item.calculateAverage()}</td>
                <td class="d-flex justify-content-between">
                    <button onclick="showStudent('${item.id}')" class="btn btn-outline-primary btn-sm"><i class="fa fa-eye"></i></button>
                    <button onclick="editStudent('${item.id}')" class="btn btn-outline-success btn-sm"><i class="fa fa-edit"></i></button>
                    <button onclick="deleteStudent('${item.id}')" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
            `;
            tbodyContent += content;
        })
        document.getElementById("tbStudentList").innerHTML = tbodyContent;
    }
    renderEmployeeList = (EmployeeList) => {
        let tbodyContent = ``;
        EmployeeList.map(item=>{
            let content =  `
            <tr>
                <th>${item.id}</th>
                <td>${item.name}</td>
                <td>${item.email}</td>
                <td>${item.address}</td>
                <td>${item.workingDays}</td>
                <td>${item.wage}</td>
                <td>${item.calculateSalary()}</td>
                <td class="d-flex justify-content-between">
                    <button onclick="showEmployee('${item.id}')" class="btn btn-outline-primary btn-sm"><i class="fa fa-eye"></i></button>
                    <button onclick="editEmployee('${item.id}')" class="btn btn-outline-success btn-sm"><i class="fa fa-edit"></i></button>
                    <button onclick="deleteEmployee('${item.id}')" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
            `;
            tbodyContent += content;
        })
        console.log(tbodyContent);
        document.getElementById("tbEmployeeList").innerHTML = tbodyContent;
    }
    renderCustomerList = (CustomerList) => {
        let tbodyContent = ``;
        CustomerList.map(item=>{
            let content =  `
            <tr>
                <th>${item.id}</th>
                <td>${item.name}</td>
                <td>${item.email}</td>
                <td>${item.address}</td>
                <td>${item.company}</td>
                <td>${item.invoice}</td>
                <td>${item.feedback}</td>
                <td class="d-flex justify-content-between">
                    <button onclick="showCustomer('${item.id}')" class="btn btn-outline-primary btn-sm"><i class="fa fa-eye"></i></button>
                    <button onclick="editCustomer('${item.id}')" class="btn btn-outline-success btn-sm"><i class="fa fa-edit"></i></button>
                    <button onclick="deleteCustomer('${item.id}')" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
            `;
            tbodyContent += content;
        })
        document.getElementById("tbCustomerList").innerHTML = tbodyContent;
    }
}