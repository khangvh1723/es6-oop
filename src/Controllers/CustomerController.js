import { ListPerson } from "../Models/listPerson.js";
import { CustomerType } from "../Forms/CustomerType.js";
import { TableComponent } from "../Components/TableComponent.js";

const componentManager = new TableComponent();
const entityManager = new ListPerson();
const formManager = new CustomerType();

export class CustomerController {
    showAllCustomer() {
        const customers = entityManager.findAll();
        componentManager.renderCustomerList(customers);
    }
    showCustomer(id) {
        const customer = entityManager.find(id);
        formManager.showDataForm(customer);
        $('#myModal').modal('show');
    }
    postCustomer() {
        let customer = formManager.getDataForm();
        entityManager.addPerson(customer);
        componentManager.renderCustomerList(entityManager.persons);
    }
    updateCustomer() {
        let customer = formManager.getDataForm();
        entityManager.updatePerson(customer);
        componentManager.renderCustomerList(entityManager.persons);
        $('#myModal').modal('hide');
    }
    deleteCustomer(id) {
        entityManager.removePerson(id);
        componentManager.renderCustomerList(entityManager.persons);
    }
}