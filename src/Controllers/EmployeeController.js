import { ListPerson } from "../Models/listPerson.js";
import { EmployeeType } from "../Forms/EmployeeType.js";
import { TableComponent } from "../Components/TableComponent.js";

const componentManager = new TableComponent();
const entityManager = new ListPerson();
const formManager = new EmployeeType();

export class EmployeeController {
    showAllEmployee() {
        const employees = entityManager.findAll();
        componentManager.renderEmployeeList(employees);
    }
    showEmployee(id) {
        const employee = entityManager.find(id);
        formManager.showDataForm(employee);
        $('#myModal').modal('show');
    }
    postEmployee() {
        let employee = formManager.getDataForm();
        entityManager.addPerson(employee);
        componentManager.renderEmployeeList(entityManager.persons);
    }
    updateEmployee() {
        let employee = formManager.getDataForm();
        entityManager.updatePerson(employee);
        componentManager.renderEmployeeList(entityManager.persons);
        $('#myModal').modal('hide');
    }
    deleteEmployee(id) {
        entityManager.removePerson(id);
        componentManager.renderEmployeeList(entityManager.persons);
    }
}