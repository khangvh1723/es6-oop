import { ListPerson } from "../Models/listPerson.js";
import { StudentType } from "../Forms/StudentType.js";
import { TableComponent } from "../Components/TableComponent.js";

const componentManager = new TableComponent();
const entityManager = new ListPerson();
const formManager = new StudentType();

export class StudentController {
    showAllStudent() {
        const students = entityManager.findAll();
        componentManager.renderStudentList(students);
    }
    showStudent(id) {
        const student = entityManager.find(id);
        formManager.showDataForm(student);
        $('#myModal').modal('show');
    }
    postStudent() {
        let student = formManager.getDataForm();
        entityManager.addPerson(student);
        componentManager.renderStudentList(entityManager.persons);
    }
    updateStudent() {
        let student = formManager.getDataForm();
        entityManager.updatePerson(student);
        componentManager.renderStudentList(entityManager.persons);
        $('#myModal').modal('hide');
    }
    deleteStudent(id) {
        entityManager.removePerson(id);
        componentManager.renderStudentList(entityManager.persons);
    }
}