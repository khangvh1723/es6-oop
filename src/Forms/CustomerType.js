import { Customer } from "../Models/Customer.js";

export class CustomerType {
    getDataForm() {
        let id = document.getElementById("id").value;
        let name = document.getElementById("name").value;
        let email = document.getElementById("email").value;
        let address = document.getElementById("address").value;
        let role = document.getElementById("role").value;
        let company = document.getElementById("company").value;
        let invoice = document.getElementById("invoice").value*1;
        let feedback = document.getElementById("feedback").value;
        let customer = new Customer(id, name, address, email, role, company, invoice, feedback);
        return customer;
    }
    showDataForm(person) {
        document.getElementById("id").value = person.id;
        document.getElementById("name").value = person.name;
        document.getElementById("email").value = person.email;
        document.getElementById("address").value = person.address;
        document.getElementById("role").value = person.role;
        document.getElementById("role").disabled = true;
        document.getElementById("company").value = person.company;
        document.getElementById("invoice").value = person.invoice;
        document.getElementById("feedback").value = person.feedback;
    }
}