import { Employee } from "../Models/Employee.js";

export class EmployeeType {
    constructor() {
        this.entityManager = new Employee();
    }
    getDataForm() {
        let id = document.getElementById("id").value;
        let name = document.getElementById("name").value;
        let email = document.getElementById("email").value;
        let address = document.getElementById("address").value;
        let role = document.getElementById("role").value;
        document.getElementById("role").disabled = false;
        let workingDays = document.getElementById("workingDays").value*1;
        let wage = document.getElementById("wage").value*1;
        let employee = new Employee(id, name, address, email, role, workingDays, wage);
        return employee;
    }
    showDataForm(person) {
        document.getElementById("id").value = person.id;
        document.getElementById("name").value = person.name;
        document.getElementById("email").value = person.email;
        document.getElementById("address").value = person.address;
        document.getElementById("role").value = person.role;
        document.getElementById("role").disabled = true;
        document.getElementById("workingDays").value = person.workingDays;
        document.getElementById("wage").value = person.wage;
    }
}