import { Student } from "../Models/Student.js";
export class StudentType {
    getDataForm() {
        let id = document.getElementById("id").value;
        let name = document.getElementById("name").value;
        let email = document.getElementById("email").value;
        let address = document.getElementById("address").value;
        let role = document.getElementById("role").value;
        let math = document.getElementById("math").value*1;
        let physics = document.getElementById("physics").value*1;
        let chemistry = document.getElementById("chemistry").value*1;
        let student = new Student(id, name, address, email, role, math, physics, chemistry);
        return student;
    }
    showDataForm(person) {
        document.getElementById("id").value = person.id;
        document.getElementById("name").value = person.name;
        document.getElementById("email").value = person.email;
        document.getElementById("address").value = person.address;
        document.getElementById("role").value = person.role;
        document.getElementById("role").disabled = true;
        document.getElementById("math").value = person.math;
        document.getElementById("physics").value = person.physics;
        document.getElementById("chemistry").value = person.chemistry;
    }
}