import {  StudentController } from "./Controllers/StudentController.js";
import { EmployeeController } from "./Controllers/EmployeeController.js";
import { CustomerController } from "./Controllers/CustomerController.js";
import { FormDetailComponent } from "./Components/FormDetailComponent.js";

const studentController = new StudentController();
const employeeController = new EmployeeController();
const customerController = new CustomerController();
const formDetailComponent = new FormDetailComponent();
window.selectForm = () => {
    const role = document.getElementById("role").value;
    switch(role) {
        case '1':
            formDetailComponent.renderStudentForm();
        break;
        case '2': 
            formDetailComponent.renderEmployeeForm();
        break;
        case '3':
            formDetailComponent.renderCustomerForm();
        break;
    }
}
window.showAllStudent = studentController.showAllStudent;

window.createStudent= studentController.postStudent;

window.showStudent = (id) => studentController.showStudent(id);

window.editStudent = (id) => studentController.showStudent(id);

window.updateStudent = studentController.updateStudent;

window.deleteStudent = (id) => studentController.deleteStudent(id);

// Employee

window.showAllEmployee = employeeController.showAllEmployee;

window.createEmployee = employeeController.postEmployee;

window.showEmployee = (id) => employeeController.showEmployee(id);

window.editEmployee = (id) => employeeController.showEmployee(id);

window.updateEmployee = employeeController.updateEmployee;

window.deleteEmployee = (id) => employeeController.deleteEmployee(id);

// Customer

window.showAllCustomer = customerController.showAllCustomer;

window.createCustomer = customerController.postCustomer;

window.showCustomer = (id) => customerController.showCustomer(id);

window.editCustomer = (id) => customerController.showCustomer(id);

window.updateCustomer = customerController.updateCustomer;

window.deleteCustomer = (id) => customerController.deleteCustomer(id);