import Person from "./Person.js"

export class Customer extends Person {
    constructor(_id, _name, _address, _email, _role, _company, _invoice, _feedback)
    {
        super(_id, _name, _address, _email, _role)
        this.company = _company;
        this.invoice = _invoice;
        this.feedback = _feedback;
    }
}