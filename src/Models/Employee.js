import Person from "./Person.js"

export class Employee extends Person {
    constructor(_id, _name, _address, _email, _role, _workingDays, _wage)
    {
        super(_id, _name, _address, _email, _role)
        this.workingDays = _workingDays;
        this.wage = _wage;
    }
    calculateSalary() {
        return this.workingDays * this.wage;
    }
}