// import { Student } from "./Student";
// import { Employee } from "./Employee";
// import { Customer } from "./Customer";

export class ListPerson {
    constructor() {
        this.persons = [];
    }

    addPerson(person) {
        this.persons.push(person);
    }
    removePerson(id) {
        const index = this.persons.findIndex(item => item.id == id);
        if(index > -1) {
            this.persons.splice(index, 1);
        }
    }
    findAll() {
        return this.persons;
    }
    find(id) {
        const index = this.persons.findIndex(item => item.id == id);
        return this.persons[index];
    }
    updatePerson(person) {
        console.log(person);
        const index = this.persons.findIndex(item => item.id == person.id);
        this.persons[index] = person;
    }
}