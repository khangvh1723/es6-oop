export default class Person {
    constructor(_id, _name, _address, _email, _role)
    {
        this.id = _id;
        this.name = _name;
        this.address = _address;
        this.email = _email;
        this.role = _role;
    }
}