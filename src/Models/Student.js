import Person from "./Person.js"

export class Student extends Person {
    constructor(_id, _name, _address, _email, _role, _math, _physics, _chemistry)
    {
        super(_id, _name, _address, _email, _role)
        this.math = _math;
        this.physics = _physics;
        this.chemistry = _chemistry;
    }
    calculateAverage() {
        return (this.math + this.physics + this.chemistry) / 3;
    }
}